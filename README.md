# Working tools

## Getting started

There are two tools for now : 
- one to randomly open picture from a folder and its subfolders.
- one to randomly open a weblink of a frame from movie-screencaps.com 

## Description
In this project, I'll share tools I'm personally using to work.

## Installation
Packages may be needed. 
At this time you'll need to run python to make those tools work.

## Usage
Use it to get images for references or daily drawing like me.

## Support
As I'm not a good developper, I can't offer any good support but I may try :/

## Contributing
I'm open to any contributions.

## Authors and acknowledgment
Thank you all ! You that shared scripts, tips, tutorials ! Can't many any of this without grapping some lines on internet.
Thanks to movie-screencaps.com and their awesome work !


## License
Feel free to use everything you want ^^
Keep me in the look if you make awesome things with thoses poor lines.
