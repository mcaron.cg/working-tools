# Open a random picture from folder

import os
import random
import glob

def random_pic_in_folder(dir_path):
    # print('Finding pictures inside : ', dir_path)
    # print(os.path.isdir(dir_path))
    files_list = []
    for file_path in glob.glob(dir_path, recursive=True):
        print(file_path)
        if os.path.isfile(file_path):
            if file_path.lower().endswith(('.png', '.jpg', '.jpeg', '.tiff', '.bmp', '.gif')):
                files_list.append(file_path)

    print(files_list)
    return (files_list)

def open_random_pic_in_folder(dir_path):
    # print('Opening random frame inside : ', dir_path)
    os.startfile(get_random_path(dir_path))

def get_random_path(dir):
    dir_path = os.listdir(dir)

    if len(dir_path) == 0:
        return get_random_path(dir)

    chosen = os.path.join(dir, random.choice(dir_path))
    print('Chosen path is : ', chosen)

    if os.path.isfile(chosen):
        print('Chosen file :', chosen)
        if chosen.lower().endswith(('.png', '.jpg', '.jpeg', '.tiff', '.bmp', '.gif', '.JPG')):
            print('Chosen file validated', chosen)
            return chosen
        else :
            print('Not a good file :', chosen)
            return get_random_path(dir)
    else:
        print('Chosen dir :', chosen)
        return get_random_path(chosen)



if __name__ == '__main__':
    dir_path = 'T:/pictures/'
    open_random_pic_in_folder(dir_path)
    # print('The one :', get_random_path(dir_path))
