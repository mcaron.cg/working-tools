# pip install bs4
# pip install hhtplib2

import os
import httplib2
import random
from bs4 import BeautifulSoup, SoupStrainer

url='https://movie-screencaps.com/movie-directory/'

def get_links(url):
    http = httplib2.Http()
    response, content = http.request(url)

    links=[]

    for link in BeautifulSoup(content, features='html.parser').find_all('a', href=True):
        if not '/category/' in str(link):
            if not '/tag/' in str(link):
                if 'movie-screencaps.com' in str(link):
                    links.append(link['href'])

    # for link in links:
    #     print(link)

    selected_link = random.choice(links)
    return selected_link

def open_movie_frame():
    selected_movie = get_links(url)
    print(selected_movie)

    selected_page = get_links(selected_movie)
    print(selected_page)

    try :
        selected_frame = get_links(selected_page)
        print(selected_frame)
    except :
        selected_frame = selected_page

    os.startfile(selected_frame)

