import sys
from toolbox.drawing_tools import random_frame_movie_screencaps, random_pic_from_folder
from PySide2.QtWidgets import QApplication, QWidget, QMainWindow, QPushButton, QTabWidget, QLabel, QVBoxLayout, QLineEdit

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("Max's toolbox")

        # layout = QVBoxLayout()

        tabs = QTabWidget()
        tabs.setTabPosition(QTabWidget.North)
        tabs.setMovable(False)
        drawing_tab = QWidget()
        maya_tab = QWidget()

        tabs.addTab(drawing_tab,'Drawing')
        tabs.addTab(maya_tab, 'Maya')

        drawing_tab.layout = QVBoxLayout()
        movie_frame_btn = QPushButton('Random movie frame')
        movie_frame_btn.setCheckable(True)
        movie_frame_btn.clicked.connect(self.open_random_movie_frame)
        drawing_tab.layout.addWidget(movie_frame_btn)

        path_label = QLabel()
        path_label.setText('Folder path :')
        drawing_tab.layout.addWidget(path_label)

        self.pic_folder = QLineEdit()
        self.pic_folder.setText('T:/Pictures/')
        drawing_tab.layout.addWidget(self.pic_folder)

        pic_folder_btn = QPushButton('Random picture from folder')
        pic_folder_btn.setCheckable(True)
        pic_folder_btn.clicked.connect(self.open_random_pic_in_folder)
        drawing_tab.layout.addWidget(pic_folder_btn)

        drawing_tab.setLayout(drawing_tab.layout)

        self.setCentralWidget(tabs)

    def open_random_movie_frame(self):
        random_frame_movie_screencaps.open_movie_frame()
        print('Opening frame...')

    def open_random_pic_in_folder(self):
        target_path = self.pic_folder.text()
        print(target_path)
        random_pic_from_folder.open_random_pic_in_folder(target_path)
        print('Opening picture...')

def launch_UI():
    app = QApplication(sys.argv)

    window = MainWindow()
    window.show()

    sys.exit(app.exec_())


if __name__ == '__main__':
    launch_UI()